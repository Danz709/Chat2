class FileProcesser:
    def __init__(self, path):
        self.path = path

    def read(self, lines_to_read=None):
        opened_file = open(self.path, '')
        content = opened_file.read()
        content_lines = content.splitlines()
        content_lines_final = []
        if lines_to_read is None:
            for i in lines_to_read:
                content_lines_final.append(content_lines[i])

        else:
            for i in content_lines.__len__():
                content_lines_final.append(content_lines[i])

        opened_file.close()
        return content_lines_final

    def add_content(self, string_to_add):
        opened_file = open(self.path, 'w')
        opened_file.write(string_to_add)
        opened_file.close()
        opened_file = open(self.path, 'r')
        content = opened_file.read().splitlines()
        opened_file.close()
        return content
