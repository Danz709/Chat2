import socket
import threading
from Consts import HOST, PORT


class Server:
    def __init__(self):
        self.connections = []
        self.threads = []
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.open_server()
        self.open_threads()

    def open_server(self):
        self.socket.bind((HOST, PORT))
        self.socket.listen(50)

    def open_threads(self):
        new_conn_thread = threading.Thread(target=self.get_new_connection)
        self.threads.append(new_conn_thread)
        new_conn_thread.start()

    def get_new_connection(self):

        while True:
            # Get new connection
            conn, address = self.socket.accept()
            self.connections.append((conn, address))

            print 'Accepted connection from: {0}'.format(address)

            # Message existing connections to new one
            conn.send('*Now connected to chat: \n')
            for current_conn, current_address in self.connections:
                conn.send(str(current_address[1]) + '\n')

            # Send to everybody message of new connection
            for current_conn, current_address in self.connections:
                if current_address is not address:
                    current_conn.send("*" + str(address[1]) + ' Has connected to chat* \n')

            # Start new interact thread for new connection
            interact_thread = threading.Thread(target=self.interact, args=(conn, address,))
            self.threads.append(interact_thread)
            interact_thread.start()

    def interact(self, conn, address):

        conn.send('Welcome to chat. You are now connected as {0}. Enter /Username to send private message.'.format(
            str(address[1])))

        while True:

            is_public_msg = True
            client_msg = conn.recv(1024)
            private_message_receiver = str(client_msg).partition(' ')[0]  # Take first word of message

            # Check if message is private and send to receiver
            for current_conn, current_address in self.connections:
                if "/" + str(current_address[1]) == private_message_receiver:
                    private_msg = "Private ({0}): {1}".format(address[1], str(client_msg).partition(' ')[2])
                    current_conn.send(private_msg)
                    is_public_msg = False
                    break

            # If private message sent to unavailable user, you get a warning
            if (client_msg[:1] == '/') & (is_public_msg == True):
                conn.send('User isn\'t connected.')
                is_public_msg = False

            # Send public message
            if is_public_msg:
                for current_conn, current_address in self.connections:
                    if current_address is not address:
                        current_conn.send(str(address[1]) + ": " + client_msg)
