class ProcessData:
    def __init__(self):
        pass

    def write_data(self, data):
        raise NotImplementedError

    def read_data(self):
        raise NotImplementedError
