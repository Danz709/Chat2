import socket
import threading
from Consts import HOST, PORT


class Client:
    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.open_connection()
        self.threads = []
        self.open_threads()

    def open_connection(self):
        self.socket.connect((HOST, PORT))

    def open_threads(self):
        send_message_thread = threading.Thread(target=self.send_message)
        self.threads.append(send_message_thread)
        send_message_thread.start()

        get_message_thread = threading.Thread(target=self.get_server_message)
        self.threads.append(get_message_thread)
        get_message_thread.start()

    def send_message(self):
        while True:
            client_msg = raw_input()
            self.socket.send(client_msg)

    def get_server_message(self):
        while True:
            server_msg = self.socket.recv(1024)
            print server_msg
